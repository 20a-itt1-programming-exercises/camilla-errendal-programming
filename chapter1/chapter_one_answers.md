Exercise 1: What is the function of the secondary memory in a computer?
c) Store information for the long term, even beyond a power cycle

Exercise 2: What is a program?
A sequence of statements made to do something. A string of instructions to be executed one by one. 

Exercise 3: What is the difference between a compiler and an interpreter?
Translating a program from high-level to low-level language
print vs running a high-level program by translating one line at a time

Exercise 4: Which of the following contains “machine code”?
a) The Python interpreter

Exercise 5: What is wrong with the following code:
>>> primt 'Hello world!' File "<stdin>", line 1 primt 'Hello world!'
^
SyntaxError: invalid syntax

Print is spelled wrong

Exercise 6: Where in the computer is a variable such as “x” stored after the following Python line finishes?
x = 123
b) Main Memory

Exercise 7: What will the following program print out:
x = 43 x=x+1 print(x)
b) 44

Exercise 8: Explain each of the following using an example of a hu- man capability: 
(1) Central processing unit = 
(2) Main Memory = Top of mind / what you are currently concerned with
(3) Secondary Memory = Back of the head / what you where concerned with 2 weeks ago
(4) Input Device = Senses, impressions
(5) Output Device = Movements, sounds 

Exercise 9: How do you fix a “Syntax Error”?
You check for typos and fix your code
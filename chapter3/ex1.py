#Rewrite your pay computation to give the employee 1.5 times the hourly rate for hours worked above 40 hours.

hours = float(input('Working hours:'))
rate = float(input("Basic hourly rate:"))
workweek = 40

if (hours) > workweek:
	overtime = (hours) - workweek
	overtimepay = (rate) * 1.5
	pay = (hours - overtime) * rate + overtime * overtimepay
	print("Overtime worked:",(overtime))
	print("Overtime pay:",(overtimepay))
else:
	pay = hours * rate
print("total pay:",(pay))
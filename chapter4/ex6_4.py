#Exercise 6: Rewrite your pay computation with time-and-a-half for over- time and create a function called computepay which takes two parameters


hours = float(input('Working hours:'))
rate = float(input("Basic hourly rate:"))
workweek = 40


def computepay(hours,rate):
    if hours > workweek:
	    overtime = hours - workweek
	    overtimepay = rate * 1.5
	    pay = (hours - overtime) * rate + overtime * overtimepay
	    
    else:
        pay = hours * rate

    return (pay)     

pay = computepay(hours, rate)

print (pay)
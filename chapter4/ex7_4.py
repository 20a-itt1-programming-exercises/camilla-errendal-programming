#Exercise 7: Rewrite the grade program from the previous chapter using a function called computegrade that takes a score as its parameter and returns a grade as a string.


def computegrade(score):
	if score < 0 or score > 1:
		print("Score is out of range")
	elif score >= 0.9:
		return "Grade A"
	elif score >= 0.8:
		return "Grade B"
	elif score >= 0.7:
		return "Grade C"
	elif score >= 0.6:
		return "Grade D"
	elif score < 0.6:
		return "Grade F"

try:
	score = float(input("Enter score: "))
except:
	print("Bad score")
	quit()

print(computegrade(score))
